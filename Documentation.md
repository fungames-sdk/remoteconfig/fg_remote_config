# Remote Config

## Introduction

The Remote Config plugin is used to perform ab tests, but also to set ingame variables from a server so that we can modify them without having to update the application.

Even if you do not foresee conducting AB tests, or define variables remotely, we strongly recommend using this feature from the beginning. You will be able to define default values that won't be affected until we create the remote configuration or AB tests on our servers.

## Integration Steps

1) **"Install"** or **"Upload"** FG Remote Config plugin from the FunGames Integration Manager in Unity, or download it from here.

2) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

## Scripting API

To initialize your remote variables, you must first create the **FG RemoteConfig Settings** asset (_Create > FunGames > FGRemoteConfigSettings_). Once done, set the default values in the "Custom Default Values" section.

![](_source/remoteConfig_CustomValues.png)

You can also set Default values by script using the FGRemoteConfig.AddDefaultValue(...) method but you have to make sure that the function is called on Awake().

If you want to override in code default values that has already been added, you must call the FGRemoteConfig.OverrideDefaultValue(...) method in the Awake() function of a new script (not in the SDK directly !). The script must be attached to a GameObject **in the same scene** where you added the FG prefabs.

Note that fetch is now done automatically at FG initialization so there is nothing else to do. If you want to attach some custom script when fetch is complete you can subscribe to the _FGRemoteConfig.Callbacks.OnInitialized_ callback.